<?php
use Jenssegers\Agent\Agent;

function profileCompletion(){
      $user = DB::table('users_ib')->where('uuid',Session::get('user')->uuid)->first();
      $bank = DB::table('banks')->where('uuid',Session::get('user')->uuid)->orderby('id','desc')->first();
      $profileMeter = 0;
      $profileScore = 0;

      if(!empty($user->username)){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if(!empty($user->name)){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if($user->email_verification == 'verified'){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if(!empty($user->whatsapp)){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if(!empty($user->dob)){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if($user->photoKTP != '/images/upload-default.jpg'){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if($user->photoTabungan != '/images/upload-default.jpg'){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if(!empty($bank->bank_name)){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if(!empty($bank->account_name)){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      if(!empty($bank->account_number)){
        $profileMeter++;
        $profileScore++;
      }else{
        $profileMeter++;
      }

      $profileCompletion = number_format($profileScore/$profileMeter*100,0,'.','.');

      return $profileCompletion;
}

function profile(){
  $user = DB::table('users_ib')->where('uuid',Session::get('user')->uuid)->first();

  return $user;
}

function accountmt4($mt4){
  $mt4 = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$mt4)->first();
  return $mt4;
}

function uuid(){
  $uuid = Session::get('user')->uuid;
  return $uuid;
}

function bank(){
  $count = DB::table('banks')->where('uuid',Session::get('user')->uuid)->count();
  if($count < 1){
    $insert = DB::table('banks')->insert([
      'uuid' => Session::get('user')->uuid
    ]);
    $bank = DB::table('banks')->where('uuid',Session::get('user')->uuid)->orderby('id','desc')->first();
    return $bank;
  }else{
    $bank = DB::table('banks')->where('uuid',Session::get('user')->uuid)->orderby('id','desc')->first();
    return $bank;
  }
}

function currency(){
  $converter = DB::table('converter')->orderby('id','asc')->get();
  return $converter;
}

function currencyRate($cur){
  $converter = DB::table('converter')->where('id',$cur)->first();
  return $converter;
}

function converter($name,$value){
  $converter = DB::table('converter')->where('id',$name)->first();
  $converted = $value / $converter->cur_ask;
  return $converted;
}

function newSession(){
  $login = DB::table('users_ib')->where('uuid',Session::get('user')->uuid)->first();
  $priv = DB::table("cms_privileges")->where("id", $login->id_cms_privileges)->first();
  $roles = DB::table('cms_privileges_roles')->where('id_cms_privileges', $login->id_cms_privileges)->join('cms_moduls', 'cms_moduls.id', '=', 'id_cms_moduls')->select('cms_moduls.name', 'cms_moduls.path', 'is_visible', 'is_create', 'is_read', 'is_edit', 'is_delete')->get();
  $menus = DB::table('cms_menus_privileges')->where('cms_menus_privileges.id_cms_privileges', $login->id_cms_privileges)->join('cms_menus', 'cms_menus.id', 'cms_menus_privileges.id_cms_menus')->get();

  Session::put('user', $login);
  Session::put('priv', $priv);
  Session::put('modul', $roles);
  Session::put('menu', $menus);
}

function accounts(){
  $myAccounts = DB::table('typeMT5AvailabeAccount')->where('uuid',profile()->uuid)->orderby('process_at','desc')->get();
  return $myAccounts;
}

function mt5accounts(){
  $myAccounts = DB::table('typeMT5AvailabeAccount')->where('status','approved')->where('uuid',profile()->uuid)->orderby('process_at','desc')->get();
  return $myAccounts;
}

function mt4accounts(){
  $myAccounts = DB::table('typeMT4AvailabeAccount')->where('status','approved')->where('uuid',profile()->uuid)->orderby('process_at','desc')->get();
  return $myAccounts;
}

function account($mt5_id){
  $myAccount = DB::table('typeMT5AvailabeAccount')->where('mt5_id',$mt5_id)->first();
  return $myAccount;
}

function account4($mt4_id){
  $myAccount = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$mt4_id)->first();

  return $myAccount;
}

function detilBank($ask){
  $bankdetil = DB::table('bank_segre')->where('namaBank',$ask)->first();
  return $bankdetil;
}

function agent(){
  $agent = new Agent();

  if($agent->isPhone()){
    $screen = "Phone";
  }elseif($agent->isTablet()){
    $screen = "Tablet";
  }elseif($agent->isDesktop()){
    $screen = "Desktop";
  }
  $platform = $agent->platform();
  $version_platform = $agent->version($platform);
  $device = $agent->device();
  $browser = $agent->browser();
  $version_browser = $agent->version($browser);
  $languages = serialize($agent->languages());
  $robot = $agent->robot();
  $ip = request()->ip();
  return $agent;
}

function menuSignals(){
  $category = DB::table('signal_category')->where('categoryStatus','active')->orderby('id','desc')->get();
  return $category;
}
