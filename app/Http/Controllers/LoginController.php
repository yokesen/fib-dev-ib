<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Cookie;
use Session;
use Illuminate\Support\Facades\Hash;
use Alert;

class LoginController extends Controller
{
    public function submitLogin(Request $request){

      $rules = [
          'g-recaptcha-response' => 'required|captcha',
          'email' => 'required|string|min:6',
          'password' => 'required|string|min:5|max:20'
      ];

      $messages = [
          'g-recaptcha-response.required' => 'Kesalahan : Kamu harus lulus uji Captcha ini',
          'g-recaptcha-response.captcha' => 'Kesalahan : Kamu harus lulus uji Captcha ini',
          'email.required' => 'Kesalahan : Email/Username wajib diisi!',
          'email.email' => 'Kesalahan : Email/Username tidak valid!',
          'password.required' => 'Kesalahan : Password-mu masih kosong nih!',
          'password.min' => 'Kesalahan : Password-mu minimal harus 8 huruf/angka!',
          'password.max' => 'Kesalahan : Password-mu terlalu panjang!'
      ];

      $validator = Validator::make($request->all(), $rules, $messages);

      if($validator->fails()){
          return redirect()->back()->withErrors($validator)->withInput($request->all());
      }

      $login = DB::table('users_ib')->where('email',$request->email)->first();
      if(!$login){

        $login = DB::table('users_ib')->where('username',$request->email)->first();
        if(!$login){
          Alert::warning( 'Wah ga bisa login nih!!','Kalau Kamu lupa password, coba pakai fitur Lost Password ya!!')->showConfirmButton('Baik!', '#DB1430');
          return redirect()->back();
        }
      }

      if(Hash::check($request->password,$login->password)){
        $priv = DB::table("cms_privileges")->where("id", $login->id_cms_privileges)->first();
        $roles = DB::table('cms_privileges_roles')->where('id_cms_privileges', $login->id_cms_privileges)->join('cms_moduls', 'cms_moduls.id', '=', 'id_cms_moduls')->select('cms_moduls.name', 'cms_moduls.path', 'is_visible', 'is_create', 'is_read', 'is_edit', 'is_delete')->get();
        $menus = DB::table('cms_menus_privileges')->where('cms_menus_privileges.id_cms_privileges', $login->id_cms_privileges)->join('cms_menus', 'cms_menus.id', 'cms_menus_privileges.id_cms_menus')->get();
        $remember_token = time().$login->uuid;
        $remember = DB::table('users_ib')->where('email',$request->email)->update([
          'remember_token' => $remember_token
        ]);

        if ($login->id_cms_privileges < 10) {
          Alert::warning( 'Not Enough Privilleges!!','Hubungi customer service '.env('APP_NAME').'!!')->showConfirmButton('Baik!', '#DB1430');
          return redirect()->back();
        }


        Cookie::queue('remember_token', $remember_token, 31536000);
        Session::put('user', $login);
        Session::put('priv', $priv);
        Session::put('modul', $roles);
        Session::put('menu', $menus);
        toast('Signed in successfully','success')->timerProgressBar();
        
        return redirect()->route('viewDashboard');
      }else{
        Alert::warning( 'Wah ga bisa login nih!!','Kalau Kamu lupa password, coba pakai fitur Lost Password ya!!')->showConfirmButton('Baik!', '#DB1430');
        return redirect()->back();
      }

    }
}
