<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;
use Validator;
use Alert;
use Illuminate\Support\Facades\Http;

class TradingAccountController extends Controller
{
  public function viewTradingAccount(){
    $menu = 'viewTradingAccount';
    /*-------------------*/
    $accounts = DB::table('typeMT4AvailabeAccount')
                  ->join('users_cabinet','users_cabinet.uuid','typeMT4AvailabeAccount.uuid')
                  ->where('typeMT4AvailabeAccount.parent',profile()->id)
                  ->orderby('typeMT4AvailabeAccount.process_at','desc')
                  ->select(
                    'users_cabinet.username',
                    'typeMT4AvailabeAccount.typeAccount',
                    'typeMT4AvailabeAccount.mt4_id'
                    )
                  ->paginate(20);
    return view('platform.trading-account',compact('menu','accounts'));
  }

  public function createAccountMt4(Request $request){

      $rules = [
          'accountType' => 'required|max:20'
      ];

      $messages = [
        'accountType.required' => 'loh seharusnya ada pilihan akunnya, tapi kok kosong ya?',
        'accountType.max' => 'salah isian nih?',
      ];

      $validator = Validator::make($request->all(), $rules, $messages);

      if($validator->fails()){
          alert()->error('Wah Gawat','Ada yang salah nih dengan permintaan akunmu.')->showConfirmButton('Coba lagi!', '#DB1430');
          return redirect()->back()->withErrors($validator)->withInput($request->all());
      }

      $group = DB::table('typeMT4GroupList')->where('typeAccount',$request->accountType)->first();
      $gn = $group->groupName;

      $max = DB::table('typeMT4AvailabeAccount')->where('mt4_id','LIKE',$gn.'%')->where('nocan',0)->max('mt4_id');
      if ($max) {
        $accountId = $max + 1;
      }else{
        $accountId = $gn*10000;
      }


      $user = DB::table('users_cabinet')->where('uuid',$request->uuid)->first();
      $name = $user->name;
      $time = time();

      $p_name = substr($name,0,3);
      $p_time = substr($time,-4);
      $password = $p_name.$p_time;




    $client = DB::table('users_cabinet')->where('uuid',$request->uuid)->first();
    $typeAccount = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->mt4_id)->first();
    $accountCategory = DB::table('typeMT4Accounts')->where('namaAccount',$request->accountType)->where('categoryAccount',profile()->id)->first();

    $login = $accountId;
    $groupAccount = urlencode($request->kodeGroup);
    $leverage = $accountCategory->accountLeverage;
    $name = $client->name;
    $email = $client->email;
    $address = $client->address;
    $city = $client->city;
    $phone = preg_replace("/[^0-9]/", '', $client->whatsapp);

    $group = "real%5c".$groupAccount;
    //dd($request->all(),$login,$group,$name,$email,$address,$city,$phone,$leverage);
    $response = Http::post(env('BRIDGE_END_POINT').'/go/create-akun',[
      'login' => $login,
      'password' => $password,
      'group' => $group,
      'leverage' => $leverage,
      'name' => $name,
      'email' => $email,
      'phone' => $phone,
      'secret' => env('BRIDGE_KEY'),
      'address' => $address,
      'city' => $city
    ]);

    $results = $response->json();

    if ($results['status']) {
      if ($results['data']['retcode'] == "0 Done") {
        $insert = DB::table('typeMT4AvailabeAccount')->insert([
          'typeAccount' => $request->accountType,
          'parent' => $user->parent,
          'uuid' => $request->uuid,
          'mt4_id' => $accountId,
          'tipe_deposit' => '10',
          'status' => 'approved',
          'password' => $password,
          'kodeGroup' => $request->kodeGroup,
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s')
        ]);

        Alert::success( 'Success','Akun Created!'.$accountId)->showConfirmButton('OK', '#DB1430');

        return redirect()->back();
      }else{
        Alert::warning( 'ERROR','PEMBUATAN  Akun GAGAL reason : '.$results['data']['retcode'])->showConfirmButton('OK', '#DB1430');

        return redirect()->back();
      }
    }else{
      $error = $results['errors'][0];

      $msg = "";
      foreach ($error as $k => $value) {

        foreach ($value as $val) {
          $msg .= $val.", ";
        }
      }
      Alert::warning( 'ERROR','PEMBUATAN Akun GAGAL Reason : '.$msg)->showConfirmButton('OK', '#DB1430');
      return redirect()->back();
    }

      return redirect()->back();
    }
}
