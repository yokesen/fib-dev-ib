<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class BalanceSheetController extends Controller
{
  public function weekly(){
    $menu = 'viewBalance';
    /*-------------------*/
    $user = DB::table('users_ib')->where('id',profile()->id)->first();
    $statements = DB::table('statement_balance_sheet')->where('parent',$user->id)->orderby('id','asc')->get();
    $accountType = DB::table('typeMT4MasterTemplate')->where('masterStatus','active')->get();
    $accountYes = DB::table('typeMT4Accounts')->where('categoryAccount',$user->id)->where('status','!=','inactive')->get();
    $deposits = DB::table('partner_deposits')->where('uuid',$user->uuid)->orderby('id','desc')->paginate(7);
    $bank = DB::table('banks')->where('uuid',$user->uuid)->orderby('id','desc')->first();
    return view('ib.admin.my-business.balance-statement',compact('menu','user','statements','accountType','accountYes','deposits','bank'));
  }

  public function tradingStatement(){
    $menu = 'viewBalance';
    $weeklyClosedStatements = DB::table('statement_partner_weekly')->where('parent',profile()->id)->whereNotNull('weekoty')->orderby('id','desc')->get();
    /*-------------------*/
    return view('ib.admin.my-business.trading-statement',compact('menu','weeklyClosedStatements'));
  }

  public function showTradingStatementClient($uuid,$week,$year){
    $menu = 'partnerList';
    $usermenu = 'showTradingStatement';
    $user = DB::table('users_ib')->where('uuid',$uuid)->first();
    $weeklyClosedStatements = DB::table('statement_client_weekly')->join('users_cabinet','users_cabinet.uuid','statement_client_weekly.uuid')->where('statement_client_weekly.parent',$user->id)->where('statement_client_weekly.weekoty',$week)->where('statement_client_weekly.year',$year)->orderby('statement_client_weekly.id','desc')->get();
    return view('ib.admin.my-business.trading-statement-client',compact('usermenu','menu','user','weeklyClosedStatements'));
  }

  public function showTradingStatementAccount($uuidp,$uuid,$week,$year){
    $menu = 'partnerList';
    $usermenu = 'showTradingStatement';
    $user = DB::table('users_ib')->where('uuid',$uuidp)->first();
    $weeklyClosedStatements = DB::table('raw_data_deal')->where('uuid',$uuid)->where('lotweek',$week)->where('lotyear',$year)->where('Entry','1')->where('Action','<','2')->orderby('Time','desc')->get();
    $weeklyBalanceStatements = DB::table('raw_data_deal')->where('uuid',$uuid)->where('lotweek',$week)->where('lotyear',$year)->where('Action','2')->orderby('Time','desc')->get();
    $distinct = DB::table('raw_data_position')->where('uuid',$uuid)->where('lotweek',$week)->where('lotyear',$year)->distinct()->select('Position')->get();
    $weeklyFloatingStatements = [];
    foreach ($distinct as $v) {
      $arr = DB::table('raw_data_position')->where('Position',$v->Position)->orderby('humanTime','desc')->first();
      $weeklyFloatingStatements[] = $arr;
    }
    return view('ib.admin.my-business.trading-statement-account',compact('usermenu','menu','user','weeklyClosedStatements','weeklyFloatingStatements','weeklyBalanceStatements'));
  }

  public function commission(){
    $menu = 'viewBalance';
    /*-------------------*/
    $lots = DB::table('lot_week_by_parent')->where('parent',profile()->id)->orderby('id','desc')->get();
    return view('ib.admin.my-business.commission',compact('menu','lots'));
  }

  public function DailySettlement(){
    $menu = 'viewBalance';
    /*-------------------*/
    return view('platform.my-business.daily-settlement',compact('menu'));
  }

  public function Equity(){
    $menu = 'viewBalance';
    /*-------------------*/
    return view('platform.my-business.equity',compact('menu'));
  }
}
