<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Jenssegers\Agent\Agent;
use Cookie;
use Session;
use Illuminate\Support\Facades\Hash;

class LeadController extends Controller
{
  public function viewLead(){
    $menu = 'viewLead';
    /*-------------------*/
    $users = DB::table('users_cabinet')->orderby('id','desc')->where('parent',profile()->id)->paginate(20);
    return view('ib.admin.my-client.leads',compact('menu','users'));
  }

  public function viewAddLead(){
    $menu = 'viewLead';
    /*-------------------*/

    return view('platform.add-lead',compact('menu'));
  }

  public function processAddLead(Request $request){
    $rules = [
        'name' => 'required|min:2|max:50',
        'email' => 'required|email|unique:users_cabinet',
        'whatsapp' => 'required|min:10',
        'password' => 'required|min:5'
    ];

    $validator = Validator::make($request->all(), $rules);

    if($validator->fails()){
        //dd($rules,$request->all(),$validator,$validator->fails(),$errors);
        return redirect()->back()->withErrors($validator)->withInput($request->all());
    }

    $agent = new Agent();
    if($agent->isPhone()){
      $screen = "Phone";
    }elseif($agent->isTablet()){
      $screen = "Tablet";
    }elseif($agent->isDesktop()){
      $screen = "Desktop";
    }
    $platform = $agent->platform();
    $version_platform = $agent->version($platform);
    $device = $agent->device();
    $browser = $agent->browser();
    $version_browser = $agent->version($browser);
    $languages = serialize($agent->languages());
    $robot = $agent->robot();
    $ip = request()->ip();
    $uuid = sha1($request->email).time();

    $whatsapp = preg_replace("/[^0-9]/", "", $request->phone );
    /*CREATE USERNAME*/
    $username3 = explode('@',$request->email)[0];
    $check3 = DB::table('users_cabinet')->where('username',$username3)->count();
    if($check3 == 0){
      $username = $username3;
    }else{
      $username2 = $username3.date('md');
      $check2 = DB::table('users_cabinet')->where('username',$username2)->count();
      if($check2 == 0){
        $username = $username2;
      }else{
        $username = '';
      }
    }
    /*CREATE USERNAME*/

    $insert = DB::table('users_cabinet')->insertGetId([
      'name' => $request->name,
      'username' => $username,
      'email' => $request->email,
      'email_validation' => 'new',
      'email_verification' => 'new',
      'password' => Hash::make($request->password),
      'phone' => $request->whatsapp,
      'whatsapp' => $request->whatsapp,
      'providerId' => $uuid,
      'providerOrigin' => 'add manual lead',
      'id_cms_privileges' => '7',
      'parent' => profile()->id,
      'uuid' => $uuid,
      'ipaddress' => $ip,
      'screen' => $screen,
      'platform' => $platform,
      'platformVersion' => $version_platform,
      'device' => $device,
      'browser' => $browser,
      'browserVersion' => $version_browser,
      'language' => $languages,
      'robot' => $robot,
      'status' => 'lead'
    ]);

    return redirect()->route('viewLead');
  }
}
