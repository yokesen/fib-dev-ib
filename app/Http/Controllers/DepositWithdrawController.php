<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;
use Validator;
use Alert;
use Illuminate\Support\Facades\Http;

class DepositWithdrawController extends Controller
{
    public function directDeposit(Request $request){

      $amount = preg_replace("/[^0-9]/", "", $request->amount);
      $approved_amount = $amount/10000;

      $response = Http::post(env('BRIDGE_END_POINT').'/go/deposit',[
        'secret' => env('BRIDGE_KEY'),
        'login' => $request->accountmt5,
        'amount' => $approved_amount,
        'parentName' => profile()->username
      ]);
      $results = $response->json();

      if ($results['status']) {
        $bankDetail = detilBank($request->transferTo);
        if ($request->photo) {
          $photo = $request->photo;
        }else{
          $photo = '/images/upload-default.jpg';
        }

        $insert = DB::table('deposits')->insert([
          'uuid' => $request->uuid,
          'amount' => $amount,
          'tipe_deposit' => $request->tipeDeposit,
          'currency' => 'IDR',
          'approved_amount' => $approved_amount,
          'currency_rate' => 10000,
          'photo' => $photo,
          'status' => 'approved',
          'process_by' => $results['ticket'],
          'reason' => "Deposit by ".profile()->name,
          'from_bank' => $request->bank_name,
          'from_name' => $request->account_name,
          'from_rekening' => $request->account_number,
          'to_bank' => $bankDetail->namaBank,
          'to_name' => $bankDetail->namaRekening,
          'to_rekening' => $bankDetail->nomorRekening,
          'metatrader' => $request->accountmt5
        ]);

        $checkTipe = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->accountmt5)->first();
        $type = $checkTipe->tipe_deposit;
        if ($type == 0) {
          $update = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->accountmt5)->update([
            'tipe_deposit' => $request->tipeDeposit
          ]);
        }
      }
        Alert::success( 'Success','Deposit! US$ '.$approved_amount.' ke account '.$request->accountmt5)->showConfirmButton('OK', '#DB1430');
      return redirect()->back();
    }

    public function directWithdrawal(Request $request){

      $response = Http::post(env('BRIDGE_END_POINT').'/go/withdrawal',[
        'secret' => env('BRIDGE_KEY'),
        'login' => $request->accountmt5,
        'amount' => $request->amount * -1,
        'parentName' => profile()->username
      ]);
      $results = $response->json();

      if ($results['status']) {
        $metatrader = accountmt4($request->accountmt5);
        $bank = DB::table('banks')->where('uuid',$request->uuid)->first();

        $insert = DB::table('withdrawals')->insert([

          'uuid' => $request->uuid,
          'metatrader' => $request->accountmt5,
          'amount' => $request->amount,
          'tipe_deposit' => $metatrader->tipe_deposit,
          'currency' => '',
          'currency_rate' => 1,
          'approved_amount' => 0,
          'status' => 'pending',
          'bank_name' => $bank->bank_name,
          'account_name' => $bank->account_name,
          'account_number' => $bank->account_number

        ]);
          Alert::success( 'Success','Withdrawal! - US$ '.$request->amount.' dari account '.$request->accountmt5)->showConfirmButton('OK', '#DB1430');
      }else{
          Alert::success( 'GAGAL','possibly not enough money')->showConfirmButton('OK', '#DB1430');
      }

      return redirect()->back();
    }
}
