<div class="card mb-6 mb-xl-9">
  <!--begin::Header-->
  <div class="card-header border-0">
    <div class="card-title">
      <h2>Referral List</h2>
    </div>
  </div>
  <!--end::Header-->
  <!--begin::Body-->
  <div class="card-body py-6">
    <div class="table-responsive">
      <!--begin::Table-->
      <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
        <!--begin::Table head-->
        <thead>
          <tr class="fw-bolder text-muted">
            <th>Nama</th>
            <th>Detail</th>
            <th>Status</th>
          </tr>
        </thead>
        <!--end::Table head-->
        <!--begin::Table body-->
        <tbody id="table-body">
          @foreach ($referral as $x => $value)
            @php
            $role = DB::table('cms_privileges')->where('id',$value->id_cms_privileges)->first();
            $parent = DB::table('users_cabinet')->where('id',$value->parent)->select('username','uuid')->first();
            @endphp
            <tr>
              <td>
                <div class="d-flex align-items-center">
                  <div class="symbol symbol-45px me-5">
                    <img src="{{$value->photo}}" alt="" />
                  </div>
                  <div class="d-flex justify-content-start flex-column">
                    <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary fs-6">{{$value->name}}</a>
                    <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->username}}</span>
                  </div>
                </div>
              </td>
              <td>
                <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->email}}</a>
                <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->phone}}</span>
                <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->whatsapp}}</span>
              </td>
              <td>
                <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$role->name}}</a>
              </td>
            </tr>
          @endforeach
        </tbody>
        <!--end::Table body-->
      </table>
      <!--end::Table-->
      {{$referral->links()}}
    </div>
  </div>
  <!--end::Body-->
</div>
