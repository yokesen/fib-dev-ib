<!--begin::Aside Menu-->
<div class="hover-scroll-overlay-y my-2 py-5 py-lg-8" id="kt_aside_menu_wrapper" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer"
	data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">
	<!--begin::Menu-->
	<div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="#kt_aside_menu" data-kt-menu="true">
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'dashboard' ? 'active' : ''}}" href="{{route('viewDashboard')}}">
				<span class="menu-icon">
					<i class="bi bi-house fs-3"></i>
				</span>
				<span class="menu-title">Dashboard</span>
			</a>
		</div>


		<div class="menu-item">
			<div class="menu-content pt-8 pb-2">
				<span class="menu-section text-muted text-uppercase fs-8 ls-1">MY CLIENT</span>
			</div>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewLead' ? 'active' : ''}}" href="{{route('viewLead')}}">
				<span class="menu-icon">
					<i class="bi bi-people fs-3"></i>
				</span>
				<span class="menu-title">My Leads</span>
			</a>
			<a class="menu-link {{ $menu == 'viewMyClient' ? 'active' : ''}}" href="{{route('viewMyClient')}}">
				<span class="menu-icon">
					<i class="bi bi-people fs-3"></i>
				</span>
				<span class="menu-title">My Client</span>
			</a>
		</div>

		<div class="menu-item">
			<div class="menu-content pt-8 pb-2">
				<span class="menu-section text-muted text-uppercase fs-8 ls-1">MY BUSINESS</span>
			</div>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewBalanceStatement' ? 'active' : ''}}" href="{{route('viewBalanceStatement')}}">
				<span class="menu-icon">
					<i class="bi bi-hdd-network fs-3"></i>
				</span>
				<span class="menu-title">Balance Statement</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewTradingStatement' ? 'active' : ''}}" href="{{route('viewTradingStatement')}}">
				<span class="menu-icon">
					<i class="bi bi-pie-chart fs-3"></i>
				</span>
				<span class="menu-title">Trading Statement</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewCommissionWeek' ? 'active' : ''}}" href="{{route('viewCommissionWeek')}}">
				<span class="menu-icon">
					<i class="bi bi-award fs-3"></i>
				</span>
				<span class="menu-title">Commission</span>
			</a>
		</div>
		{{-- <div class="menu-item">
			<a class="menu-link {{ $menu == 'viewMyClient' ? 'active' : ''}}" href="{{route('viewMyBusinessEquity')}}">
				<span class="menu-icon">
					<i class="bi bi-bar-chart-line fs-3"></i>
				</span>
				<span class="menu-title">Equity</span>
			</a>
		</div> --}}

		{{-- <div class="menu-item">
			<a class="menu-link {{ $menu == 'viewLead' ? 'active' : ''}}" href="{{route('viewLead')}}">
				<span class="menu-icon">
					<i class="bi bi-person-plus fs-3"></i>
				</span>
				<span class="menu-title">Leads</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewContact' ? 'active' : ''}}" href="{{route('viewContact')}}">
				<span class="menu-icon">
					<i class="bi bi-telephone-outbound fs-3"></i>
				</span>
				<span class="menu-title">Contact</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewPotential' ? 'active' : ''}}" href="{{route('viewPotential')}}">
				<span class="menu-icon">
					<i class="bi bi-person-check fs-3"></i>
				</span>
				<span class="menu-title">Potential</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewLosing' ? 'active' : ''}}" href="{{route('viewLosing')}}">
				<span class="menu-icon">
					<i class="bi bi-trash fs-3"></i>
				</span>
				<span class="menu-title">Trash Bin</span>
			</a>
		</div> --}}

		{{-- <div class="menu-item">
			<div class="menu-content pt-8 pb-2">
				<span class="menu-section text-muted text-uppercase fs-8 ls-1">MY STATEMENT</span>
			</div>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewTradingAccount' ? 'active' : ''}}" href="{{route('viewTradingAccount')}}">
				<span class="menu-icon">
					<i class="bi bi-hdd-network fs-3"></i>
				</span>
				<span class="menu-title">Trading Account</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewStatement' ? 'active' : ''}}" href="{{route('viewStatement')}}">
				<span class="menu-icon">
					<i class="bi bi-pie-chart fs-3"></i>
				</span>
				<span class="menu-title">Statement</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewCommission' ? 'active' : ''}}" href="{{route('viewCommission')}}">
				<span class="menu-icon">
					<i class="bi bi-award fs-3"></i>
				</span>
				<span class="menu-title">Commission</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewProfitLoss' ? 'active' : ''}}" href="{{route('viewProfitLoss')}}">
				<span class="menu-icon">
					<i class="bi bi-bar-chart-line fs-3"></i>
				</span>
				<span class="menu-title">Reconsile</span>
			</a>
		</div> --}}
		<div class="menu-item">
			<div class="menu-content pt-8 pb-2">
				<span class="menu-section text-muted text-uppercase fs-8 ls-1">MARGIN IN-OUT</span>
			</div>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewNetMargin' ? 'active' : ''}}" href="{{route('viewNetMargin')}}">
				<span class="menu-icon">
					<i class="bi bi-journal-check fs-3"></i>
				</span>
				<span class="menu-title">Net Margin</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewMarginIn' ? 'active' : ''}}" href="{{route('viewMarginIn')}}">
				<span class="menu-icon">
					<i class="bi bi-journal-plus fs-3"></i>
				</span>
				<span class="menu-title">Margin In</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewMarginOut' ? 'active' : ''}}" href="{{route('viewMarginOut')}}">
				<span class="menu-icon">
					<i class="bi bi-journal-minus fs-3"></i>
				</span>
				<span class="menu-title">Margin Out</span>
			</a>
		</div>


	</div>
	<!--end::Menu-->
</div>
