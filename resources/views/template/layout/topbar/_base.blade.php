
<!--begin::Toolbar wrapper-->
<div class="d-flex align-items-stretch flex-shrink-0">
	<!--begin::Search-->
	<div class="d-flex align-items-stretch ms-1 ms-lg-3">

		<!--layout-partial:layout/search/_base.html-->

	</div>
	<!--end::Search-->
	<!--begin::Activities-->
	<div class="d-flex align-items-center ms-1 ms-lg-3">

	</div>
	<!--end::Activities-->

	<!--begin::Notifications-->
	<div class="d-flex align-items-center ms-1 ms-lg-3 text-white">
		
	</div>
	<!--end::Notifications-->
	<!--begin::User-->
	<div class="d-flex align-items-center ms-1 ms-lg-3" id="kt_header_user_menu_toggle">
		<!--begin::Menu-->
		<div class="cursor-pointer symbol symbol-30px symbol-md-40px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
			<img src="{{Session::get('user')->photo}}" alt="{{Session::get('user')->name}}" />
		</div>

		@include('template.layout.topbar.partials._user-menu')

		<!--end::Menu-->
	</div>
	<!--end::User -->
	<!--begin::Heaeder menu toggle-->
	@if(Session::get('user')->id_cms_privileges == 8 || Session::get('user')->id_cms_privileges == 9 || Session::get('user')->id_cms_privileges == 10)
		<div class="d-flex align-items-center d-lg-none ms-2 me-n3" title="Show header menu">
			<div class="btn btn-icon btn-active-light-primary" id="kt_header_menu_mobile_toggle">
				<i class="bi bi-text-left fs-1 text-white"></i>
			</div>
		</div>
	@endif
	<!--end::Heaeder menu toggle-->
</div>
<!--end::Toolbar wrapper-->
