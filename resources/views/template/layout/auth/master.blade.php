<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head>
    <meta charset="utf-8" />
		<title>{{ENV('APP_NAME')}}</title>
		<meta name="robots" content="noindex, nofollow" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="shortcut icon" href="{{url('/')}}/images/favicon.png" />
		<meta name="csrf-token" content="{{ csrf_token() }}">
		@if (ENV('APP_PWA')=='yes')
			@laravelPWA
		@endif
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="{{url('/')}}/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="{{url('/')}}/assets/css/custom.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
		@yield('cssinline')
		<style media="screen">
    .gradient_right{
      background-color: #ff0000; /* For browsers that do not support gradients */
      background-image: linear-gradient(to top right, #cccccc,#eeeeee); /* Standard syntax (must be last) */
    }

    .gradient_left{
      background-color: #c49229; /* For browsers that do not support gradients */
      background-image: linear-gradient(to top right, #cccccc,#eeeeee); /* Standard syntax (must be last) */
    }
  </style>



	</head>
	<!--end::Head-->
  @yield('container')
					<!--begin::Footer-->
					<div class="d-flex flex-center flex-wrap fs-6 p-5 pb-0">
						<!--begin::Links-->
						<div class="d-flex flex-center fw-bold fs-6">
							<a href="#" class="text-muted text-hover-primary px-2" target="_blank">About</a>
							<a href="#" class="text-muted text-hover-primary px-2" target="_blank">Support</a>
						</div>
						<!--end::Links-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Body-->
			</div>
			<!--end::Authentication - Sign-up-->
		</div>
		<!--end::Main-->
		<!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="{{url('/')}}/assets/plugins/global/plugins.bundle.js"></script>
		<script src="{{url('/')}}/assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		@include('sweetalert::alert')
		<!--end::Javascript-->
		@yield('jsinline')
		<script type="text/javascript">
			$.ajaxSetup({
				headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
		</script>

	</body>
	<!--end::Body-->
</html>
