<header class="main-nav">
  <div class="sidebar-user text-center">
        <a class="setting-primary" href="javascript:void(0)"><i data-feather="settings"></i></a><img class="img-90 rounded-circle" src="{{profile()->photo}}"/>
        <div class="badge-bottom"><span class="badge badge-primary">New</span></div>
        <a href="user-profile"> <h6 class="mt-3 f-14 f-w-600">{{Session::get('user')->name}}</h6></a>
        <p class="mb-0 font-roboto">{{Session::get('user')->username}}</p>
        <ul>
            <li>
                <span><span class="counter">{{number_format(Session::get('user')->creditGiven,'0','.','.')}}</span>$</span>
                <p>Credit</p>
            </li>
            <li>
                <span><span class="counter">{{number_format(Session::get('user')->takingPositionPercentage,'0','.','.')}}</span>%</span>
                <p>Position</p>
            </li>
            <li>
                <span><span class="counter">{{number_format(100 - (Session::get('user')->LastKnownPercentLimit*100),'2','.','.')}}</span>%</span>
                <p>Margin</p>
            </li>
        </ul>
    </div>
    <nav>
        <div class="main-navbar">
            <div id="mainnav">
                <ul class="nav-menu custom-scrollbar">
                    <li class="back-btn">
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                    </li>
                    <li class="sidebar-main-title">
                        <div>
                            <h6>Welcome</h6>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link menu-title link-nav" href="{{route('index')}}"><i data-feather="home"></i><span>Dashboard</span></a>
                    </li>
                    <li class="sidebar-main-title">
                        <div>
                            <h6>My Business</h6>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link menu-title link-nav" href="{{route('viewBalanceStatement')}}"><i data-feather="briefcase"></i><span>Balance Statement</span></a>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link menu-title link-nav" href="{{route('viewTradingStatement')}}"><i data-feather="bar-chart-2"></i><span>Trading Statement</span></a>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link menu-title link-nav" href="{{route('viewCommissionWeek')}}"><i data-feather="award"></i><span>Commission</span></a>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="dollar-sign"></i><span>Margin In-Out</span></a>
                        <ul class="nav-submenu menu-content">
                          <li><a href="{{route('viewNetMargin')}}">Net Margin</a></li>  
                          <li><a href="{{route('viewMarginIn')}}">Margin In</a></li>
                          <li><a href="{{route('viewMarginOut')}}">Margin Out</a></li>
                        </ul>
                    </li>
                    <li class="sidebar-main-title">
                        <div>
                            <h6>My Client</h6>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link menu-title link-nav" href="{{route('viewLead')}}"><i data-feather="user-plus"></i><span>Leads</span></a>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link menu-title link-nav" href="{{route('viewMyClient')}}"><i data-feather="users"></i><span>Clients</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
