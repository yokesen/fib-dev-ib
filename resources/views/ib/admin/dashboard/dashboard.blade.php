@extends('ib.layouts.admin.master')

@section('title', 'Dashboard')

@push('breadcrumb')
@endpush

@push('css')
@endpush
    @section('content')
      @component('ib.components.breadcrumb')
            @slot('breadcrumb_title')
                <h3>Welcome</h3>
            @endslot
            <li class="breadcrumb-item">Dashboard</li>
            <li class="breadcrumb-item active">Welcome {{Session::get('user')->name}}</li>
        @endcomponent
      <!-- Container-fluid starts-->
      <div class="container-fluid dashboard-default-sec">

      </div>
      <!-- Container-fluid Ends-->
    @push('scripts')

    @endpush
@endsection
