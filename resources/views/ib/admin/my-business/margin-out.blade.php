@extends('ib.layouts.admin.master')

@section('title', 'Margin Out')

@push('breadcrumb')
@endpush

@push('css')
@endpush
    @section('content')
      @component('ib.components.breadcrumb')
            @slot('breadcrumb_title')
                <h3>My Business</h3>
            @endslot
            <li class="breadcrumb-item">Margin Out</li>
            <li class="breadcrumb-item active">{{Session::get('user')->name}}</li>
        @endcomponent
      <!-- Container-fluid starts-->
      <div class="container-fluid dashboard-default-sec">
        <div class="row">
          <div class="col-xl-12 xl-100 box-col-12">
            <div class="card">
              <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                <h5>MARGIN IN</h5>

              </div>
              <div class="card-body">
                <div class="user-status table-responsive">
                  <table class="table table-bordernone">
                    <thead>
                      <tr>
                        <th class="min-w-100px">Account Trading</th>
                        <th>Time</th>
                        <th>Ticket</th>
                        <th>Symbol</th>
                        <th>Type</th>
                        <th class="text-end">Price</th>
                        <th class="text-end">Closed</th>
                        <th class="text-end">Floating</th>
                        <th class="text-end">Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($margin as $w)
                        @php
                        if ($w->Profit < 0) {
                          $warna = 'success';
                          $icon = 'fa-check-circle';
                        }elseif($w->Profit > 0){
                          $warna = 'danger';
                          $icon = 'fa-times-circle';
                        }else{
                          $warna = 'primary';
                          $icon = 'fa-clock';
                        }


                        @endphp
                        <!--begin::Table row-->
                        <tr {{$w->ModifyFlags == '1' ? 'class=table-danger' : ''}}>
                          <!--begin::Invoice=-->
                          <td>
                            {{$w->Login}}
                          </td>
                          <td>
                            {{$w->humanTime}}
                          </td>
                          <td colspan="6" class="text-end">
                            {{$w->Comment}}
                          </td>
                          <!--begin::Action=-->
                          <td class="text-end">{{number_format($w->Profit,'2','.',',')}}</td>
                          <!--end::Action=-->
                        </tr>
                        <!--end::Table row-->
                      @endforeach
                    </tbody>
                  </table>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Container-fluid Ends-->
    @push('scripts')

    @endpush
@endsection
