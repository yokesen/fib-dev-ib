@extends('ib.layouts.admin.master')

@section('title', 'Internal Transfer')

@push('breadcrumb')
@endpush

@push('css')
@endpush
    @section('content')
      @component('ib.components.breadcrumb')
            @slot('breadcrumb_title')
                <h3>My Business</h3>
            @endslot
            <li class="breadcrumb-item">Internal Transfer</li>
            <li class="breadcrumb-item active">{{Session::get('user')->name}}</li>
        @endcomponent
      <!-- Container-fluid starts-->
      <div class="container-fluid dashboard-default-sec">

      </div>
      <!-- Container-fluid Ends-->
    @push('scripts')

    @endpush
@endsection
