@extends('ib.layouts.admin.master')

@section('title', 'Commission')

@push('breadcrumb')
@endpush

@push('css')
@endpush
    @section('content')
      @component('ib.components.breadcrumb')
            @slot('breadcrumb_title')
                <h3>My Business</h3>
            @endslot
            <li class="breadcrumb-item">Commission</li>
            <li class="breadcrumb-item active">{{Session::get('user')->name}}</li>
        @endcomponent
      <!-- Container-fluid starts-->
      <div class="container-fluid dashboard-default-sec">
        <div class="row">
          <div class="col-xl-12 xl-100 box-col-12">
            <div class="card">
              <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                <h5>COMMISSION</h5>

              </div>
              <div class="card-body">
                <div class="user-status table-responsive">
                  <table class="table table-bordernone">
                    <thead>
                      <tr>
                        <th scope="col">Week.</th>
                        <th scope="col">Periode</th>
                        <th scope="col">Lot</th>
                        <th scope="col">Fee</th>
                        <th scope="col">Percentage</th>
                        <th scope="col">Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($lots as $w)
                        @php
                        $week = $w->week;
                        $year = $w->year;

                        $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
                        $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
                        $date_for_monday = date( 'd-m-Y', $timestamp_for_monday );
                        $date_for_friday = date( 'd-m-Y', strtotime('friday ',$timestamp_for_monday) );
                        @endphp
                        <!--begin::Table row-->
                        <tr>
                          <!--begin::Invoice=-->
                          <td>
                            Week-{{$w->week}}
                          </td>
                          <!--end::Invoice=-->
                          <td>{{$date_for_monday}} to {{$date_for_friday}}</td>
                          <!--begin::Status=-->
                          <td class="text-primary">
                            {{number_format($w->volume,'2','.',',')}}
                          </td>
                          <!--end::Status=-->
                          <!--begin::Amount=-->
                          <td>
                            2.5$
                          </td>
                          <!--end::Amount=-->
                          <!--begin::Date=-->

                          <!--end::Date=-->
                          <!--begin::Action=-->
                          <td>{{profile()->commissionPercentage}}%</td>
                          <td class="text-primary">{{number_format($w->volume*2.5*10000*profile()->commissionPercentage/100,'0','.',',')}}</td>

                          <!--end::Action=-->
                        </tr>
                        <!--end::Table row-->
                      @endforeach
                    </tbody>
                  </table>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Container-fluid Ends-->
    @push('scripts')

    @endpush
@endsection
