@extends('ib.layouts.admin.master')

@section('title', 'Balance Statement')

@push('breadcrumb')
@endpush

@push('css')
@endpush
    @section('content')
      @component('ib.components.breadcrumb')
            @slot('breadcrumb_title')
                <h3>My Business</h3>
            @endslot
            <li class="breadcrumb-item">Balance Statement</li>
            <li class="breadcrumb-item active">{{Session::get('user')->name}}</li>
        @endcomponent
      <!-- Container-fluid starts-->
      <div class="container-fluid dashboard-default-sec">
        <div class="row">
          <div class="col-xl-12 xl-100 box-col-12">
            <div class="card">
              <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                <h5>BALANCE STATEMENT</h5>

              </div>
              <div class="card-body">
                <div class="user-status table-responsive">
                  <table class="table table-bordernone">
                    <thead>
                      <tr>
                        <th>Tanggal</th>
                        <th>Trx Source</th>
                        <th>Trx Type</th>
                        <th>Description</th>
                        <th class="text-end">Amount</th>
                        <th class="text-end">Type</th>
                        <th class="text-end">Balance</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($statements as $value)
                        <tr>
                          <td>{{$value->tanggal}}</td>
                          <td>{{$value->sumber}}</td>
                          <td>{{$value->tipe_transaksi}}</td>
                          <td>{{$value->keterangan}}</td>
                          <td class="text-end">{{number_format($value->mutasi,'0','.','.')}}</td>
                          <td class="text-end">{{$value->tipe_mutasi}}</td>
                          <td class="text-end">{{number_format($value->saldo,'0','.','.')}}</td>
                        </tr>

                      @endforeach
                    </tbody>
                  </table>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Container-fluid Ends-->
    @push('scripts')

    @endpush
@endsection
