@extends('ib.layouts.admin.master')

@section('title', 'Trading Statement')

@push('breadcrumb')
@endpush

@push('css')
@endpush

@section('content')
  @component('ib.components.breadcrumb')
    @slot('breadcrumb_title')
      <h3>My Business</h3>
    @endslot
    <li class="breadcrumb-item">Trading Statement</li>
    <li class="breadcrumb-item active">{{Session::get('user')->name}}</li>
  @endcomponent
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
      <div class="row">
        <div class="col-xl-12 xl-100 box-col-12">
          <div class="card">
            <div class="card-header pb-0 d-flex justify-content-between align-items-center">
              <h5>TRADING STATEMENT</h5>

            </div>
            <div class="card-body">
              <div class="user-status table-responsive">
                <table class="table table-bordernone">
                  <thead>
                    <tr>
                      <th scope="col">Week.</th>
                      <th scope="col">Periode</th>
                      <th scope="col">Closed</th>
                      <th scope="col">Floating</th>
                      <th scope="col">Total</th>
                      <th scope="col">Overview</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($weeklyClosedStatements as $w)
                      @php
                      if ($w->profit < 0) {
                        $warna = 'success';
                        $icon = 'fa-check-circle';
                      }elseif($w->profit > 0){
                        $warna = 'danger';
                        $icon = 'fa-times-circle';
                      }else{
                        $warna = 'primary';
                        $icon = 'fa-clock';
                      }

                      $week = $w->weekoty;
                      $year = $w->year;

                      $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
                      $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
                      $date_for_monday = date( 'd-m-Y', $timestamp_for_monday );
                      $date_for_friday = date( 'd-m-Y', strtotime('friday ',$timestamp_for_monday) );
                      @endphp
                      <!--begin::Table row-->
                      <tr>
                        <!--begin::Invoice=-->
                        <td>
                          Week-{{$w->weekoty}}
                        </td>
                        <!--end::Invoice=-->
                        <td>{{$date_for_monday}} to {{$date_for_friday}}</td>
                        <!--begin::Status=-->
                        <td class="text-{{$warna}}">
                          {{$w->profit != 0 ? number_format($w->profit,'2','.',',') : $w->profit}}
                        </td>
                        <!--end::Status=-->
                        <!--begin::Amount=-->
                        <td>
                          {{$w->floating != 0 ? number_format($w->floating,'2','.',',') : $w->floating}}
                        </td>
                        <!--end::Amount=-->
                        <!--begin::Date=-->

                        <!--end::Date=-->
                        <!--begin::Action=-->
                        <td class="text-{{$w->profit + $w->floating < 0 ? 'success' : 'danger'}}">{{number_format($w->profit + $w->floating,'2','.',',')}}</td>
                        <td>
                          <a href="{{route('viewTradingStatementClient',[profile()->uuid,$w->weekoty,$w->year])}}" class="btn btn-primary"> <i class="fa fa-eye text-white"></i> Check</a>
                        </td>

                        <!--end::Action=-->
                      </tr>
                      <!--end::Table row-->
                    @endforeach
                  </tbody>
                </table>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Container-fluid Ends-->
    @push('scripts')
    @endpush
  @endsection
