@extends('ib.layouts.admin.master')

@section('title', 'My Leads')

@push('breadcrumb')
@endpush

@push('css')
@endpush
@section('content')
  @component('ib.components.breadcrumb')
    @slot('breadcrumb_title')
      <h3>My Clients</h3>
    @endslot
    <li class="breadcrumb-item">Leads</li>
    <li class="breadcrumb-item active">{{Session::get('user')->name}}</li>
  @endcomponent
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <div class="row">
      <div class="col-xl-12 xl-100 box-col-12">
        <div class="card">
          <div class="card-header pb-0 d-flex justify-content-between align-items-center">
            <h5>LEADS</h5>

            <div> <a href="{{route('viewAddLead')}}" class="btn btn-light-primary"><i class="icon-users"></i> Add New Lead</a> </div>

          </div>
          <div class="card-body">
            <div class="user-status table-responsive">
              <table class="table table-bordernone">
                <thead>
                  <tr>
                    <th scope="col">Created Date</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Komunikasi</th>
                    <th scope="col">Provider</th>
                    <th scope="col">Status</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($users as $x => $value)
                    @php
                      $role = DB::table('cms_privileges')->where('id',$value->id_cms_privileges)->first();
                      $parent = DB::table('users_ib')->where('id',$value->parent)->select('username','uuid')->first();
                    @endphp
                    <tr>
                      <td class="f-w-600">
                        <a href="{{route('showUser',$value->uuid)}}" class="font-primary">{{$value->created_at}}</a>
                        <br>
                        <span>{{ \Carbon\Carbon::parse(strtotime($value->created_at.'+7hours'))->diffForHumans()}}</span>
                      </td>
                      <td>
                        <div class="d-flex align-items-center">
                          <div class="symbol symbol-45px me-5">
                            <img src="{{$value->photo}}" height="40" />
                          </div>
                          <div class="d-flex justify-content-start flex-column">
                            <a href="{{route('showUser',$value->uuid)}}" class="font-primary">{{$value->name}}</a>
                            <span class="font-primary">{{$value->username}}</span>
                          </div>
                        </div>
                      </td>
                      <td class="font-primary">
                        <a href="{{route('showUser',$value->uuid)}}" class="font-primary">{{$value->email}}</a>
                        <br>
                        <span class="font-primary">{{$value->whatsapp}}</span>
                      </td>
                      <td>
                        @if ($parent)
                          <a href="{{route('showUser',$parent->uuid)}}" class="font-primary">{{$parent->username}}</a>
                        @endif
                        <br>
                        <span class="font-secondary">{{$value->providerOrigin}}</span>
                      </td>
                      <td>
                        <a href="{{route('showUser',$value->uuid)}}"><div class="span badge rounded-pill pill-badge-primary">{{$role->name}}</div></a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              {{$users->links()}}
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
  @push('scripts')

  @endpush
@endsection
