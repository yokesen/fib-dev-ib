@extends('ib.admin.authentication.master')

@section('title')
  Register | Investorich
@endsection

@push('css')
@endpush

@section('content')
    <section>
	    <div class="container-fluid p-0">
	        <div class="row m-0">
	            <div class="col-12 p-0">
	                <div class="login-card">
	                    <form class="theme-form login-form" method="post" action="{{route('process-Register')}}" id="registerForm">
	                        <h4>Create your account</h4>
	                        <h6>Enter your personal details to create account</h6>
                          <div class="form-group">
	                            <label>Full Name</label>
	                            <div class="input-group">
	                                <span class="input-group-text"><i class="icon-user"></i></span>
	                                <input class="form-control" type="text" placeholder="" name="name" autocomplete="off" value="{{ old('name') }}" required {{$errors->has('name') ? 'autofocus' : ''}}/>
	                            </div>
                              @if ($errors->has('name'))
                                  <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('name') }}</h4>
                              @endif
	                        </div>
	                        <div class="form-group">
	                            <label>Email Address</label>
	                            <div class="input-group">
	                                <span class="input-group-text"><i class="icon-email"></i></span>
	                                <input class="form-control" type="email" placeholder="" name="email" autocomplete="off" value="{{ old('email') }}" required {{$errors->has('email') ? 'autofocus' : ''}}/>
	                            </div>
                              @if ($errors->has('email'))
                                  <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('email') }}</h4>
                              @endif
	                        </div>
                          <div class="form-group">
	                            <label>Whatsapp</label>
	                            <div class="input-group">
	                                <span class="input-group-text"><i class="icon-mobile"></i></span>
	                                <input class="form-control" type="text" placeholder="" name="phone" autocomplete="off" value="{{ old('phone') }}" required {{$errors->has('phone') ? 'autofocus' : ''}}/>
	                            </div>
                              @if ($errors->has('phone'))
                                  <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('phone') }}</h4>
                              @endif
	                        </div>
	                        <div class="form-group">
	                            <label>Password</label>
	                            <div class="input-group">
	                                <span class="input-group-text"><i class="icon-lock"></i></span>
	                                <input class="form-control" type="password" placeholder="" minlength="8"  name="password" autocomplete="off" required/>
	                                <div class="show-hide"><span class="show"> </span></div>
	                            </div>
                              @if ($errors->has('password'))
                                  <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('password') }}</h4>
                              @endif
	                        </div>

                          <div class="form-group">
	                            <label>Password Confirmation</label>
	                            <div class="input-group">
	                                <span class="input-group-text"><i class="icon-lock"></i></span>
	                                <input class="form-control" minlength="8" type="password" placeholder="" name="password_confirmation" autocomplete="off" required />
	                                <div class="show-hide"><span class="show"> </span></div>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <div class="checkbox">
	                                <input id="checkbox1" type="checkbox" />
	                                <label class="text-muted" for="checkbox1">Agree with <span>Privacy Policy </span></label>
	                            </div>
	                        </div>

                          @csrf
                          <div class="g-recaptcha" data-sitekey="{{ENV('NOCAPTCHA_SITEKEY')}}"></div>

                          <h4 class="text-danger mt-6" hidden id="recaptcha-error"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> Wajib buktikan kamu bukan robot!</h4>
                          <br>
                          <div class="form-group">
                              <button class="btn btn-primary btn-block btn-lg" type="submit" id="ot_button">Create Account</button>
                          </div>

	                        <p>Already have an account?<a class="ms-2" href="{{ route('viewLogin') }}">Sign in</a></p>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>


    @push('scripts')
      <script src="https://www.google.com/recaptcha/api.js?hl=id"></script>
      <script src="{{url('/')}}/assets-x/js/orbitrade/auth/recaptcha.js"></script>
      <script src="{{url('/')}}/assets-x/js/orbitrade/auth/register.js"></script>
    @endpush

@endsection
