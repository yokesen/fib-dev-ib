@extends('ib.admin.authentication.master')

@section('title')
  Login | Investorich
@endsection

@push('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/sweetalert2.css') }}">
@endpush

@section('content')
    <section>
    <div class="container-fluid p-0">

        <div class="row">
            <div class="col-12">
                <div class="login-card">

                    <form class="theme-form login-form" method="post" action="{{route('process-Login')}}" id="registerForm">
                        <img src="{{env('IMG_LOGO')}}" width="100%">
                        <br>
                        <br>
                        <h4>Login</h4>
                        <h6>Welcome back! Log in to your account.</h6>
                        <div class="form-group">
                            <label>Email Address/Username</label>
                            <div class="input-group">
                                <span class="input-group-text"><i class="icon-email"></i></span>
                                <input class="form-control {{ old('email') && !$errors->has('email') ? 'input-valid' : '' }} {{$errors->has('email') ? 'input-error' : ''}}" type="text" placeholder="" name="email" autocomplete="off" value="{{ old('email') }}" required {{$errors->has('email') ? 'autofocus' : ''}}/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <div class="input-group">
                                <span class="input-group-text"><i class="icon-lock"></i></span>
                                <input class="form-control {{old('password') ? 'input-error' : ''}}" type="password" placeholder="" minlength="5"  name="password" autocomplete="off" required/>
                                <div class="show-hide"><span class="show"> </span></div>
                            </div>
                        </div>
                        @csrf


                        <div class="form-group">
                            <div class="checkbox">
                                <input id="checkbox1" type="checkbox" />
                                <label for="checkbox1">Remember password</label>
                            </div>
                            <a class="link" href="{{ route('viewLostPassword') }}">Forgot password?</a>
                        </div>

                        <div class="g-recaptcha" data-sitekey="{{ENV('NOCAPTCHA_SITEKEY')}}"></div>

                        <h4 class="text-danger mt-6" hidden id="recaptcha-error"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> Wajib buktikan kamu bukan robot!</h4>
                        <br>
                        <div class="form-group">
                            <button class="btn btn-primary btn-block btn-lg" type="submit">Sign in</button>
                        </div>

                        <p>Don't have account?<a class="ms-2" href="{{ route('viewRegister') }}">Register Now!</a></p>
                      {{-- <div class="login-social-title">
                          <h5>Sign in with</h5>
                      </div>
                      <div class="form-group">
                          <ul class="login-social">
                              <li>
                                  <a href="{{ route('process-login-oAuth', 'google') }}" target="_blank"><i data-feather="google"></i></a>
                              </li>
                          </ul>
                      </div> --}}

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


    @push('scripts')
      <script src="https://www.google.com/recaptcha/api.js?hl=id"></script>
      <script src="{{url('/')}}/assets-x/js/orbitrade/auth/recaptcha.js"></script>
    @endpush

@endsection
