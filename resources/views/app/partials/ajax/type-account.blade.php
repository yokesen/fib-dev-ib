@foreach ($typeAccounts as $x => $account)
  <!--begin::Col-->
  <div class="col-xl-4">
    <div class="d-flex h-100 align-items-center">
      <!--begin::Option-->
      <div class="w-100 d-flex flex-column flex-center rounded-3 bg-white py-15 px-10">
        <!--begin::Heading-->
        <div class="mb-7 text-center">
          <!--begin::Title-->
          <h1 class="text-dark mb-5 fw-boldest">{{$account->namaAccount}}</h1>
          <!--end::Title-->
          <!--begin::Description-->
          <div class="text-gray-400 fw-bold mb-5">{{$account->deskripsiAccount}}</div>
          <!--end::Description-->
          <!--begin::Price-->
          <div class="text-center">
            <span class="mb-2 text-primary">$</span>
            <span class="fs-3x fw-bolder text-primary">{{$account->minimumDepo}}</span>
          </div>
          <!--end::Price-->
        </div>
        <!--end::Heading-->
        <!--begin::Features-->
        <div class="w-100 mb-10">
          <!--begin::Item-->
          <div class="d-flex flex-stack mb-5">
            <span class="fw-bold fs-6 text-gray-800 text-start pe-3">Up to 10 Active Users</span>
            <!--begin::Svg Icon | path: icons/duotone/Code/Done-circle.svg-->
            <span class="svg-icon svg-icon-1 svg-icon-success">
              <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                <path d="M16.7689447,7.81768175 C17.1457787,7.41393107 17.7785676,7.39211077 18.1823183,7.76894473 C18.5860689,8.1457787 18.6078892,8.77856757 18.2310553,9.18231825 L11.2310553,16.6823183 C10.8654446,17.0740439 10.2560456,17.107974 9.84920863,16.7592566 L6.34920863,13.7592566 C5.92988278,13.3998345 5.88132125,12.7685345 6.2407434,12.3492086 C6.60016555,11.9298828 7.23146553,11.8813212 7.65079137,12.2407434 L10.4229928,14.616916 L16.7689447,7.81768175 Z" fill="#000000" fill-rule="nonzero" />
              </svg>
            </span>
            <!--end::Svg Icon-->
          </div>
          <!--end::Item-->
          <!--begin::Item-->
          <div class="d-flex flex-stack mb-5">
            <span class="fw-bold fs-6 text-gray-800 text-start pe-3">Up to 30 Project Integrations</span>
            <!--begin::Svg Icon | path: icons/duotone/Code/Done-circle.svg-->
            <span class="svg-icon svg-icon-1 svg-icon-success">
              <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                <path d="M16.7689447,7.81768175 C17.1457787,7.41393107 17.7785676,7.39211077 18.1823183,7.76894473 C18.5860689,8.1457787 18.6078892,8.77856757 18.2310553,9.18231825 L11.2310553,16.6823183 C10.8654446,17.0740439 10.2560456,17.107974 9.84920863,16.7592566 L6.34920863,13.7592566 C5.92988278,13.3998345 5.88132125,12.7685345 6.2407434,12.3492086 C6.60016555,11.9298828 7.23146553,11.8813212 7.65079137,12.2407434 L10.4229928,14.616916 L16.7689447,7.81768175 Z" fill="#000000" fill-rule="nonzero" />
              </svg>
            </span>
            <!--end::Svg Icon-->
          </div>
          <!--end::Item-->
          <!--begin::Item-->
          <div class="d-flex flex-stack mb-5">
            <span class="fw-bold fs-6 text-gray-800">Keen Analytics Platform</span>
            <!--begin::Svg Icon | path: icons/duotone/Code/Error-circle.svg-->
            <span class="svg-icon svg-icon-1">
              <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                <path d="M12.0355339,10.6213203 L14.863961,7.79289322 C15.2544853,7.40236893 15.8876503,7.40236893 16.2781746,7.79289322 C16.6686989,8.18341751 16.6686989,8.81658249 16.2781746,9.20710678 L13.4497475,12.0355339 L16.2781746,14.863961 C16.6686989,15.2544853 16.6686989,15.8876503 16.2781746,16.2781746 C15.8876503,16.6686989 15.2544853,16.6686989 14.863961,16.2781746 L12.0355339,13.4497475 L9.20710678,16.2781746 C8.81658249,16.6686989 8.18341751,16.6686989 7.79289322,16.2781746 C7.40236893,15.8876503 7.40236893,15.2544853 7.79289322,14.863961 L10.6213203,12.0355339 L7.79289322,9.20710678 C7.40236893,8.81658249 7.40236893,8.18341751 7.79289322,7.79289322 C8.18341751,7.40236893 8.81658249,7.40236893 9.20710678,7.79289322 L12.0355339,10.6213203 Z" fill="#000000" />
              </svg>
            </span>
            <!--end::Svg Icon-->
          </div>
          <!--end::Item-->
          <!--begin::Item-->
          <div class="d-flex flex-stack mb-5">
            <span class="fw-bold fs-6 text-gray-800">Targets Timelines &amp; Files</span>
            <!--begin::Svg Icon | path: icons/duotone/Code/Error-circle.svg-->
            <span class="svg-icon svg-icon-1">
              <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                <path d="M12.0355339,10.6213203 L14.863961,7.79289322 C15.2544853,7.40236893 15.8876503,7.40236893 16.2781746,7.79289322 C16.6686989,8.18341751 16.6686989,8.81658249 16.2781746,9.20710678 L13.4497475,12.0355339 L16.2781746,14.863961 C16.6686989,15.2544853 16.6686989,15.8876503 16.2781746,16.2781746 C15.8876503,16.6686989 15.2544853,16.6686989 14.863961,16.2781746 L12.0355339,13.4497475 L9.20710678,16.2781746 C8.81658249,16.6686989 8.18341751,16.6686989 7.79289322,16.2781746 C7.40236893,15.8876503 7.40236893,15.2544853 7.79289322,14.863961 L10.6213203,12.0355339 L7.79289322,9.20710678 C7.40236893,8.81658249 7.40236893,8.18341751 7.79289322,7.79289322 C8.18341751,7.40236893 8.81658249,7.40236893 9.20710678,7.79289322 L12.0355339,10.6213203 Z" fill="#000000" />
              </svg>
            </span>
            <!--end::Svg Icon-->
          </div>
          <!--end::Item-->
          <!--begin::Item-->
          <div class="d-flex flex-stack">
            <span class="fw-bold fs-6 text-gray-800">Unlimited Projects</span>
            <!--begin::Svg Icon | path: icons/duotone/Code/Error-circle.svg-->
            <span class="svg-icon svg-icon-1">
              <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                <path d="M12.0355339,10.6213203 L14.863961,7.79289322 C15.2544853,7.40236893 15.8876503,7.40236893 16.2781746,7.79289322 C16.6686989,8.18341751 16.6686989,8.81658249 16.2781746,9.20710678 L13.4497475,12.0355339 L16.2781746,14.863961 C16.6686989,15.2544853 16.6686989,15.8876503 16.2781746,16.2781746 C15.8876503,16.6686989 15.2544853,16.6686989 14.863961,16.2781746 L12.0355339,13.4497475 L9.20710678,16.2781746 C8.81658249,16.6686989 8.18341751,16.6686989 7.79289322,16.2781746 C7.40236893,15.8876503 7.40236893,15.2544853 7.79289322,14.863961 L10.6213203,12.0355339 L7.79289322,9.20710678 C7.40236893,8.81658249 7.40236893,8.18341751 7.79289322,7.79289322 C8.18341751,7.40236893 8.81658249,7.40236893 9.20710678,7.79289322 L12.0355339,10.6213203 Z" fill="#000000" />
              </svg>
            </span>
            <!--end::Svg Icon-->
          </div>
          <!--end::Item-->
        </div>
        <!--end::Features-->
        <!--begin::Select-->
        <a href="#" class="btn btn-sm btn-primary">Select</a>
        <!--end::Select-->
      </div>
      <!--end::Option-->
    </div>
  </div>
  <!--end::Col-->
@endforeach
