<!--begin::Label-->
<label class="col-lg-4 col-form-label required fw-bold fs-6">Tipe Deposit </label>
<!--end::Label-->
<!--begin::Col-->
<div class="col-lg-8 fv-row">
  <input class="form-control form-control-lg form-control-solid input-valid" name="tipedepositfixed" value="{{$td->cur_one}}" readonly/>
  <input class="form-control form-control-lg form-control-solid input-valid" type="hidden" name="tipeDeposit" value="{{$td->id}}" required/>
</div>
<!--end::Col-->
