@extends('template.master')

@section('title','Cashier | Deposit')
@section('bc-1','Deposit')
@section('bc-2','Detail')

@section('container')

  <div id="kt_content_container" class="container">
    @include('app.partials._deposit-invoice')
  </div>
@endsection
