@extends('template.master')

@section('title','Add New Lead')
@section('metadescription','Invest to rich')
@section('metakeyword','Invest to rich')
@section('bc-1','My Client')
@section('bc-2','Add New Lead')

@section('container')
  <div id="kt_content_container" class="container">
      <div class="row">
        <form method="POST" action="{{route('processAddLead')}}" class="form" enctype="multipart/form-data">
          <div class="col-md-12">
            <!--begin::Basic info-->
            <div class="card mb-5 mb-xl-10">
              <!--begin::Card header-->
              <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
                <!--begin::Card title-->
                <div class="card-title m-0">
                  <h3 class="fw-bolder m-0">Add New Partner </h3>
                </div>
                <!--end::Card title-->
              </div>
              <!--begin::Card header-->
              <!--begin::Content-->
              <div class="card-body border-top p-9">
                <div class="row">
                  <div class="col-md-6">

                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">
                        <input class="form-control form-control-lg form-control-solid {{ old('name') && !$errors->has('name') ? 'input-valid' : '' }} {{$errors->has('name') ? 'input-error' : ''}}" type="text" placeholder="" name="name" autocomplete="off" value="{{ old('name') ? old('name') : '' }}" required {{$errors->has('name') ? 'autofocus' : ''}}/>
                        @if ($errors->has('name'))
                            <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('name') }}</h4>
                        @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Email</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">

                          <input class="form-control form-control-lg form-control-solid {{ old('email') && !$errors->has('email') ? 'input-valid' : '' }} {{$errors->has('email') ? 'input-error' : ''}}" type="email" name="email"  autocomplete="off" value="{{ old('email') ? old('email') : '' }}" required {{$errors->has('email') ? 'autofocus' : ''}}/>
                          @if ($errors->has('email'))
                              <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('email') }}</h4>
                          @endif


                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Whatsapp</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">
                        <input class="form-control form-control-lg form-control-solid {{ old('whatsapp') && !$errors->has('whatsapp') ? 'input-valid' : '' }} {{$errors->has('whatsapp') ? 'input-error' : ''}}" type="text" name="whatsapp"  autocomplete="off" value="" required {{$errors->has('whatsapp') ? 'autofocus' : ''}} required/>
                        @if ($errors->has('whatsapp'))
                            <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('whatsapp') }}</h4>
                        @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-6" data-kt-password-meter="true">

                        <!--begin::Label-->
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">Password</label>
                        <!--end::Label-->
                        <div class="col-lg-8 fv-row">
                          <!--begin::Input wrapper-->
                          <div class="position-relative mb-3">
                            <input class="form-control form-control-lg form-control-solid {{old('password') ? 'input-error' : ''}}" type="password" placeholder="" minlength="8"  name="password" autocomplete="off" required/>
                            <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                              <i class="bi bi-eye-slash fs-2"></i>
                              <i class="bi bi-eye fs-2 d-none"></i>
                            </span>
                          </div>
                          <!--end::Input wrapper-->
                          <!--begin::Meter-->
                          <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                            <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                            <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                            <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                            <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
                          </div>
                          <!--end::Meter-->
                          <!--begin::Hint-->
                          <div class="text-muted">min 8 chars with a mix of letters, numbers &amp; symbols.</div>
                          <!--end::Hint-->
                          @if ($errors->has('password'))

                              <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('password') }}</h4>
                          @endif
                        </div>
                    </div>
                    <!--end::Input group=-->


                  </div>



                </div>


                @csrf
                <!--begin::Actions-->
                <div class="card-footer d-flex justify-content-end py-6 px-9">

                  <button type="submit" class="btn btn-primary" id="ot_button">Submit</button>
                </div>
                <!--end::Actions-->
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
@endsection

@section('jsinline')

@endsection
