@extends('template.master')

@section('title','Commission')
@section('metadescription','FIB Partner the best business partner for IB')
@section('metakeyword','FIB Partner')
@section('bc-1','My Business')
@section('bc-2','Commission')

@section('container')

  <div id="kt_content_container" class="container">
    <div class="row g-3 g-xl-9 mb-5">
      <div class="col-sm-6 col-md-4">
        <!--begin::Card-->
        <div class="card h-100">
          <!--begin::Card body-->
          <div class="card-body p-9">
            <!--begin::Heading-->
            <div class="fs-2hx fw-bolder">{{number_format($totalRef,'0',',','.')}}</div>
            <div class="fs-4 fw-bold text-gray-400 mb-7">Total Referral</div>
            <!--end::Heading-->
            <!--begin::Wrapper-->
            <div class="d-flex flex-wrap">
              <!--begin::Chart-->
              <div class="d-flex flex-center h-100px w-100px me-9 mb-5">
                <canvas id="kt_project_list_chart"></canvas>
              </div>
              <!--end::Chart-->
              <!--begin::Labels-->
              <div class="d-flex flex-column justify-content-center flex-row-fluid pe-11 mb-5">
                <!--begin::Label-->
                @foreach ($data as $w => $dtRef)
                  <div class="d-flex fs-6 fw-bold align-items-center mb-3">
                    <div class="bullet me-3" style="background-color:{{$dtRef['color']}}!important"></div>
                    <div class="text-gray-400">{{$dtRef['namaPriv']}}</div>
                    <div class="ms-auto fw-bolder text-gray-700"> {{number_format($dtRef['count'],'0',',','.')}}</div>
                  </div>
                @endforeach
              </div>
              <!--end::Labels-->
            </div>
            <!--end::Wrapper-->
          </div>
          <!--end::Card body-->
        </div>
        <!--end::Card-->
      </div>
      <div class="col-sm-6 col-md-4">
        <!--begin::Budget-->
        <div class="card h-100">
          <div class="card-body p-9">
            <div class="fs-2hx fw-bolder">{{$counters ? number_format($counters->volume,'2',',','.') : 0}} Lot</div>
            <div class="fs-4 fw-bold text-gray-400 mb-7">{{date('F Y',strtotime('- 1 month'))}} Volume</div>
            {{-- <div class="fs-6 d-flex justify-content-between mb-4">
            <div class="fw-bold">Year to date</div>
            <div class="d-flex fw-bolder">{{$year ? $year->volume : 0}}</div>
          </div> --}}


          <div class="separator separator-dashed"></div>
          <div class="fs-6 d-flex justify-content-between mt-4">
            <div class="fw-bold">Daily Average</div>
            <div class="d-flex fw-bolder">{{$avgDaily ? number_format($avgDaily,'2',',','.') : 0}}</div>
          </div>
        </div>
      </div>
      <!--end::Budget-->
    </div>
    <div class="col-sm-6 col-md-4">
      <!--begin::Clients-->
      <div class="card h-100">
        <div class="card-body p-9">
          <!--begin::Heading-->
          <div class="fs-2hx fw-bolder">{{$countActive}}</div>
          <div class="fs-4 fw-bold text-gray-400 mb-7">Active Trader</div>
          <!--end::Heading-->
          <!--begin::Users group-->
          <div class="symbol-group symbol-hover mb-9">
            @if ($active7)
              @foreach ($active7 as $actv)
                <div class="symbol symbol-35px symbol-circle" data-bs-toggle="tooltip" title="{{$actv->name}}">
                  <img alt="Pic" src="{{$actv->photo}}" />
                </div>
              @endforeach
            @endif

            <a href="#" class="symbol symbol-35px symbol-circle" data-bs-toggle="modal" data-bs-target="#kt_modal_view_users">
              <span class="symbol-label bg-dark text-gray-300 fs-8 fw-bolder">+{{$restActive ? number_format($restActive,0,',','.') : '0'}}</span>
            </a>
          </div>
          <!--end::Users group-->
          <!--begin::Actions-->
          <div class="d-flex">
            <a href="{{route('viewMyClient')}}" class="btn btn-primary btn-sm me-3">See All</a>
          </div>
          <!--end::Actions-->
        </div>
      </div>
      <!--end::Clients-->
    </div>
  </div>

  <div class="row">
    <div class="col-md-12 mb-5">
      <!--begin::Statistics Widget 3-->
      <div class="card card-xl-stretch mb-xl-8">
        <div class="card-header p-10">
          <h4>Invite New Trader</h4>
          <p class="text-muted fw-bold mt-1">Simple Click Referral System</p>
        </div>
        <!--begin::Body-->
        <div class="card-body d-flex flex-column p-10">
          <div class="row">
            <div class="col-md-12">
              <div class="row mb-6">
                <!--begin::Label-->
                <label class="col-lg-2 col-form-label fw-bold fs-6">Link </label>
                <!--end::Label-->
                <!--begin::Col-->
                <div class="col-lg-10 fv-row">
                  <input type="text" id="copyIB" class="form-control form-control-lg form-control-solid" value="{{env('IMS_ACTIVE')}}/?ref={{profile()->id}}&utm_source=copylink&utm_campaign=Affiliate&utm_medium=super&utm_content=master">
                </div>
              </div>

              <div class="card-footer d-flex justify-content-end py-6 mt-5">
                <button style="background: #0088cc; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-5" onclick="copyClipboard()">Copy Link</button>
              </div>
            </div>
          </div>
          <!-- share row -->

          <div class="row">

            <div class="col-md-6 col-xs-12">
              <a id="whatsapp" style="background: #4FCE5D; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=whatsapp&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=whatsapp&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=whatsapp&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=whatsapp&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}, 'newwindow', 'width=600,height=500'); return false;"><i class="fab fa-whatsapp" aria-hidden="true"></i> Whatsapp</a>
            </div>

            <div class="col-md-6 col-xs-12">
              <a id="facebook" style="background: #3c5b9b; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=facebook&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=facebook&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=facebook&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=facebook&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}, 'newwindow', 'width=600,height=500'); return false;"><i class="fab fa-facebook-f" aria-hidden="true"></i> Facebook</a>
            </div>

            <div class="col-md-6 col-xs-12">
              <a id="twitter" style="background: #00acee; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=twitter&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=twitter&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=twitter&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=twitter&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}, 'newwindow', 'width=600,height=500'); return false;"><i class="fab fa-twitter" aria-hidden="true"></i> Twitter</a>
            </div>

            <div class="col-md-6 col-xs-12">
              <a id="telegram" style="background: #0088cc; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=telegram&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=telegram&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=telegram&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=telegram&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}, 'newwindow', 'width=600,height=500'); return false;"><i class="fab fa-telegram" aria-hidden="true"></i> Telegram</a>
            </div>

            <div class="col-md-6 col-xs-12">
              <a id="line" style="background: #00B900; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=lineme&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=line&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=lineme&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=line&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}, 'newwindow', 'width=600,height=500'); return false;"><i class="fab fa-line" aria-hidden="true"></i> Line</a>
            </div>

            <div class="col-md-6 col-xs-12">
              <a id="linkedin" style="background: #0e76a8; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3"
              href="window.open('https://www.linkedin.com/shareArticle?mini=true&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=line&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}&ro=false&summary=&source="
              onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=line&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}&ro=false&summary=&source=', 'newwindow', 'width=600,height=500'); return false;">
              <i class="fab fa-linkedin-in"></i> LinkedIn</a>
            </div>
          </div>
          <!-- /share row -->
        </div>
      </div>
    </div>
  </div>

  <div class="row g-6 g-xl-9">
    <!--begin::Col-->
    @foreach ($actives as $key => $actv)
      @php
      $role = DB::table('cms_privileges')->where('id',$actv->id_cms_privileges)->first();
      $Totaldeposit = DB::table('deposits')->where('uuid',$actv->uuid)->sum('approved_amount');
      @endphp
      <div class="col-md-6 col-lg-4">
        <!--begin::Card-->
        <div class="card">
          <!--begin::Card body-->
          <div class="card-body d-flex flex-center flex-column pt-12 p-9">
            <!--begin::Avatar-->
            <div class="symbol symbol-65px symbol-circle mb-5">
              <img src="{{$actv->photo}}" alt="image" />
              <div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n3 mt-n3"></div>
            </div>
            <!--end::Avatar-->
            <!--begin::Name-->
            <a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">{{$actv->name}}</a>
            <!--end::Name-->
            <!--begin::Position-->
            <div class="fw-bold text-gray-400 mb-6">{{$actv->email}}</div>
            <!--end::Position-->
            <!--begin::Info-->
            <div class="d-flex flex-center flex-wrap">
              <!--begin::Stats-->
              <div class="border border-gray-300 border-dashed min-w-80px py-3 px-4 mx-2 mb-3">
                <div class="fs-6 fw-bolder text-gray-700 text-center">{{number_format($actv->volume,2,',','.')}}</div>
                <div class="fw-bold text-gray-400 text-center">Volume {{date('M',strtotime('-1 month'))}}</div>
              </div>
              <!--end::Stats-->
              <!--begin::Stats-->
              <div class="border border-gray-300 border-dashed min-w-80px py-3 px-4 mx-2 mb-3">
                <div class="fs-6 fw-bolder text-gray-700 text-center">USD {{number_format($Totaldeposit,2,',','.')}}</div>
                <div class="fw-bold text-gray-400 text-center">Total Deposit</div>
              </div>
              <!--end::Stats-->

            </div>
            <!--end::Info-->
          </div>
          <!--end::Card body-->
        </div>
        <!--end::Card-->
      </div>
    @endforeach
    {{$actives->links()}}
  </div>
</div>

@endsection

@section('jsonpage')
<script type="text/javascript">
  function copyClipboard() {
    /* Get the text field */
    var copyText = document.getElementById("copyIB");

    /* Select the text field */
    copyText.select();

    /* Copy the text inside the text field */
    document.execCommand("copy");

    /* Alert the copied text */
    alert("Copied the text: " + copyText.value);
  }

  var t = document.getElementById("kt_project_list_chart");
  if (t) {
    var e = t.getContext("2d");
    new Chart(e, {
      type: "doughnut",
      data: {
        datasets: [{
          data: [
            @foreach ($data as $c)
              "{{$c['count']}}",
            @endforeach
          ],
          backgroundColor: [
            @foreach ($data as $b)
              "{{$b['color']}}",
            @endforeach
          ],
        }],
        labels: [
          @foreach ($data as $l)
            "{{$l['namaPriv']}}",
          @endforeach
        ]
      },
      options: {
        chart: {
          fontFamily: "inherit"
        },
        cutoutPercentage: 65,
        responsive: !0,
        maintainAspectRatio: !1,
        legend: {
          display: !1
        },
        title: {
          display: !1
        },
        animation: {
          animateScale: !0,
          animateRotate: !0
        },
        tooltips: {
          enabled: !0,
          intersect: !1,
          mode: "nearest",
          bodySpacing: 5,
          yPadding: 10,
          xPadding: 10,
          caretPadding: 0,
          displayColors: !1,
          backgroundColor: "#20D489",
          titleFontColor: "#ffffff",
          cornerRadius: 4,
          footerSpacing: 0,
          titleSpacing: 0
        }
      }
    })
  }
</script>
@endsection
