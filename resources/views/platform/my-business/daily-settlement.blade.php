@extends('template.master')

@section('title','Daily Settlement')
@section('metadescription','FIB Partner the best business partner for IB')
@section('metakeyword','FIB Partner')
@section('bc-1','My Business')
@section('bc-2','Daily Settlement')

@section('container')

  <div id="kt_content_container" class="container">
      <div class="row">
        <div class="col-md-12">
          <!--begin::Tables Widget 9-->
          <div class="card card-xxl-stretch mb-5 mb-xl-8">

            <!--begin::Body-->
            <div class="card-body py-3">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="fw-bolder text-muted">
                      <th>Date</th>
                      <th>Status</th>
                      <th>P/L Amount</th>
                      <th>Cutoff at</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody id="table-body">

                  </tbody>
                  <!--end::Table body-->
                </table>
                <!--end::Table-->
              </div>
              <!--end::Table container-->
            </div>
            <!--begin::Body-->
          </div>
          <!--end::Tables Widget 9-->
        </div>
      </div>

    </div>

@endsection
